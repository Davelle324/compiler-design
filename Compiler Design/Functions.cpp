#include "Source.cpp"
void Prog()
{
	match(key_program);						//first make sure program is being called
	if (CT == id)
	{
		ST[index].tok = prog_id;			//makes id into program id
		CT = ST[index].tok;
	}
	match(prog_id);
	match(lparen);
	idl();
	match(rparen);
	match(semicolon);
	Decls();
	sub_Decls();
	cmp_ST();
	match(D);
}
void idl()
{
	match(id);
	while (CT == comma)
	{
		match(comma);
		match(id);
	}
}
void Decls()
{
	while (CT == key_var)
	{
		match(key_var);
		idl();
		match(colon);
		Type();
		match(semicolon);
	}
}
void Type()
{
	if (CT == arrayid)
	{
		match(arrayid);
		match(LB);
		match(num);
		match(DD);
		match(RB);
		match(key_of);
		STtyp();
	}
	else
		STtyp();
}
void STtyp()
{
	switch (CT)
	{
	case key_integer:
		match(key_integer);
		break;
	case key_real:
		match(key_real);
		break;
	case key_boolean:
		match(key_boolean);
		break;
	}

}